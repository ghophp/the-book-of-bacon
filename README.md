the-book-of-bacon [![Coverage Status](https://coveralls.io/repos/bitbucket/ghophp/the-book-of-bacon/badge.svg?branch=master)](https://coveralls.io/bitbucket/ghophp/the-book-of-bacon?branch=master)
=================


This project aims to create the system that understand how to decode The Book of Bacon. The ancient manuscript is considered sacred by many. The book holds a secret message that is enconded in the following way:

- The signaling word (aka beacon word) is periodical. Every occurrence of it is separated by another 666 words in-between.​
- The first/last occurrence of the beacon word can be at any distance from the beginning/end of the text, but once it shows up it’s always at this constant interval.
- The individual words following each occurrence of the beacon word compose the secret message.

### Build [![CircleCI](https://circleci.com/bb/ghophp/the-book-of-bacon/tree/master.svg?style=svg)](https://circleci.com/bb/ghophp/the-book-of-bacon/tree/master)

```
make build
```

### Run

```
./bin/latest/the-book-of-bacon
  -interval int
    	the secret interval (eg. 6)
  -path string
    	path to book (eg. /tmp/book.txt)
```

### Dependencies

```
make deps
```

### Coverage

To see exactly the coverage into the code using the standard golang tool you can use the command bellow:

```
./script/coverage.sh --html
```