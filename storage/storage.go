package storage

import (
	"bufio"
	"fmt"
	"os"

	"bitbucket.org/ghophp/the-book-of-bacon/model"
)

type (
	Storage interface {
		ReadAll() ([]model.Line, error)
		Write(lines []string) error
	}

	FileStorage struct {
		file *os.File
	}
)

func NewFileStorage(filePath string) (*FileStorage, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	return &FileStorage{file}, nil
}

func (fs *FileStorage) ReadAll() ([]model.Line, error) {
	var (
		lines   []model.Line
		scanner = bufio.NewScanner(fs.file)
	)

	for scanner.Scan() {
		line, err := model.NewLine(scanner.Text())
		if err != nil {
			return nil, err
		}

		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}

func (fs *FileStorage) Write(lines []string) error {
	writeFile, err := os.Create(fs.file.Name() + ".reconstruct")
	if err != nil {
		return err
	}

	w := bufio.NewWriter(writeFile)
	for i, c := range lines {
		_, err := w.WriteString(fmt.Sprintf("%d # %s\n", i+1, c))
		if err != nil {
			return err
		}
	}

	err = w.Flush()
	if err != nil {
		return err
	}

	err = fs.file.Close()
	if err != nil {
		return err
	}

	return nil
}
