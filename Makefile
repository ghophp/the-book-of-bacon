.PHONY: all deps test clean build coveralls

GO ?= go
BIN_NAME=the-book-of-bacon

all: build test

deps:
	${GO} get github.com/op/go-logging

build: deps
build:
	${GO} build -o bin/latest/${BIN_NAME}

test: deps
test:
	${GO} test bitbucket.org/ghophp/the-book-of-bacon/reconstructor

coveralls:
	./script/coverage.sh --coveralls

clean:
	rm -rf bin/latest/${BIN_NAME}