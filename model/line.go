package model

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type (
	Line struct {
		Position int
		Text     string
	}

	By func(p1, p2 *Line) bool

	// LineSorter joins a By function and a slice of lines to be sorted.
	LineSorter struct {
		lines []Line
		by    func(p1, p2 *Line) bool // Closure used in the Less method.
	}
)

func NewLine(text string) (Line, error) {
	var (
		line   Line
		pieces = strings.Split(text, "#")
	)

	if len(pieces) < 2 {
		return line, fmt.Errorf("invalid line composition")
	}

	linePosition, err := strconv.Atoi(strings.TrimSpace(pieces[0]))
	if err != nil {
		return line, err
	}

	line.Position = linePosition
	line.Text = strings.TrimSpace(pieces[1])

	return line, nil
}

func (l *Line) String() string {
	return fmt.Sprintf("%d # %s", l.Position, l.Text)
}

// Sort is a method on the function type, By, that sorts the argument slice according to the function.
func (by By) Sort(lines []Line) {
	ps := &LineSorter{
		lines: lines,
		by:    by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(ps)
}

// Len is part of sort.Interface.
func (s *LineSorter) Len() int {
	return len(s.lines)
}

// Swap is part of sort.Interface.
func (s *LineSorter) Swap(i, j int) {
	s.lines[i], s.lines[j] = s.lines[j], s.lines[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *LineSorter) Less(i, j int) bool {
	return s.by(&s.lines[i], &s.lines[j])
}
