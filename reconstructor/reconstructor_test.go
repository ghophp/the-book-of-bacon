package reconstructor

import (
	"testing"

	"bitbucket.org/ghophp/the-book-of-bacon/model"
)

type (
	testCase struct {
		lines    []model.Line
		word     string
		interval int
		expect   string
	}
)

func TestProcessShouldReturnProperPhrase(t *testing.T) {
	testCases := []testCase{
		testCase{
			lines:    []model.Line{},
			interval: 6,
			expect:   "",
		},
		testCase{
			lines: []model.Line{
				model.Line{5, "this is the last line"},
				model.Line{1, "this is the first line"},
				model.Line{4, "point three in fact almost nothing matters"},
				model.Line{2, "this text does not matter point one at all"},
				model.Line{3, "this one right point two here is not important either"},
			},
			interval: 6,
			expect:   "one two three",
		},
		testCase{
			lines: []model.Line{
				model.Line{1, "Point two is the first line and"},
				model.Line{2, "point"},
				model.Line{3, "wait one right point two the point this"},
				model.Line{4, "three in fact almost no point matters"},
				model.Line{5, "this point the last line"},
			},
			interval: 6,
			expect:   "two wait this matters",
		},
		testCase{
			lines: []model.Line{
				model.Line{1, "point no is the first line and"},
				model.Line{2, "point"},
				model.Line{3, "ha one right point two the point this"},
				model.Line{4, "three in fact almost no point matters"},
				model.Line{5, "this point the last line point which"},
			},
			interval: 6,
			expect:   "no ha this matters which",
		},
		testCase{
			lines: []model.Line{
				model.Line{1, "b b b"},
				model.Line{2, "a b a"},
				model.Line{3, "b b b"},
			},
			interval: 999,
			expect:   "",
		},
	}

	for i, tc := range testCases {
		r := NewReconstructor(tc.lines, tc.interval)
		secretPhrase := r.GetSecretPhrase()
		if secretPhrase != tc.expect {
			t.Errorf("invalid secret phrase expected '%s' got '%s' for test case %d", tc.expect, secretPhrase, i)
		}
	}
}
