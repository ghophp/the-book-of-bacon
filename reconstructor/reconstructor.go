package reconstructor

import (
	"strings"

	"bitbucket.org/ghophp/the-book-of-bacon/model"
)

type (
	Reconstructor interface {
		Sort()
		GetSecretPhrase() string
		GetLines() []model.Line
	}

	DefaultReconstructor struct {
		lines          []string
		words          []string
		secretInterval int
	}
)

func NewReconstructor(lines []model.Line, secretInterval int) *DefaultReconstructor {
	current := make([]string, len(lines), len(lines))
	for _, m := range lines {
		current[m.Position-1] = m.Text
	}

	return &DefaultReconstructor{current, []string{}, secretInterval}
}

func (r *DefaultReconstructor) GetLines() []string {
	return r.lines
}

func (r *DefaultReconstructor) GetSecretPhrase() string {
	var (
		beaconWords    = map[string][]int{}
		excludeBeacons = []string{}
		secretPhrase   = []string{}
	)

	for _, line := range r.lines {
		split := strings.Split(line, " ")
		for _, w := range split {
			if len(strings.TrimSpace(w)) > 0 {
				r.words = append(r.words, strings.ToLower(w))
			}
		}
	}

	for i, w := range r.words {
		edge := (i + (r.secretInterval + 1))
		if edge < len(r.words) && w == r.words[edge] {
			if len(beaconWords[w]) <= 0 {
				beaconWords[w] = append(beaconWords[w], i)
			}
			beaconWords[w] = append(beaconWords[w], edge)
		}
	}

	for b, positions := range beaconWords {
		for i, p := range positions {
			if len(positions) <= 2 {
				excludeBeacons = append(excludeBeacons, b)
				break
			}
			if i+1 < len(positions) {
				if p+r.secretInterval+1 != positions[i+1] {
					excludeBeacons = append(excludeBeacons, b)
					break
				}
			}
		}
	}

	for _, e := range excludeBeacons {
		delete(beaconWords, e)
	}

	for _, positions := range beaconWords {
		for _, p := range positions {
			secretPhrase = append(secretPhrase, r.words[p+1])
		}
		break // use just one beacon word
	}

	return strings.Join(secretPhrase, " ")
}
