package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/ghophp/the-book-of-bacon/model"
	"bitbucket.org/ghophp/the-book-of-bacon/reconstructor"
	"bitbucket.org/ghophp/the-book-of-bacon/storage"

	"github.com/op/go-logging"
)

const (
	// LoggerPrefix holds the value of the logger prefix
	LoggerPrefix = "THE-BOOK-OF-BACON"
)

var logger = logging.MustGetLogger(LoggerPrefix)

// Log format string. Everything except the message has a custom color
// which is dependent on the log level. Many fields have a custom output
// formatting too, eg. the time returns the hour down to the milli second.
var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
)

func main() {
	logging.SetFormatter(format)

	var (
		filePath       string
		secretInterval int
		lines          []model.Line
	)

	flag.StringVar(&filePath, "path", "", "path to book (eg. /tmp/book.txt)")
	flag.IntVar(&secretInterval, "interval", 0, "the secret interval (eg. 6)")
	flag.Parse()

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		logger.Fatal("please provide a valid path for the book file")
	}

	storage, err := storage.NewFileStorage(filePath)
	if err != nil {
		log.Fatal(err)
	}

	lines, err = storage.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	var (
		rec          = reconstructor.NewReconstructor(lines, secretInterval)
		secretPhrase = rec.GetSecretPhrase()
	)

	err = storage.Write(rec.GetLines())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(fmt.Sprintf("secret message: %s", secretPhrase))
}
